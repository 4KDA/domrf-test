export interface IPlateInfo {
  width: number;
  height: number;
  depth: number;
  weight: number;
}

export const BasePlateSize: IPlateInfo = {
  width: 667,
  height: 500,
  depth: 80,
  weight: 36,
};
