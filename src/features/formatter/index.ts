export const useFormatterDecimal = (v: number): string => {
  const formatter = new Intl.NumberFormat(["ru-RU"], {
    style: "decimal",
    maximumFractionDigits: 2,
  });

  return formatter.format(v);
};
